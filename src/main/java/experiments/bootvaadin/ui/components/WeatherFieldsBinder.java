package experiments.bootvaadin.ui.components;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.data.converter.StringToIntegerConverter;
import experiments.bootvaadin.model.Weather;

public class WeatherFieldsBinder {
  Binder<Weather> binder = new Binder<>();

  public WeatherFieldsBinder(WeatherFields weatherFields) {
    binder.forField(weatherFields.getDateField())
            .asRequired("The date is required")
            .bind(
                    Weather::getDate,
                    Weather::setDate);

    binder.forField(weatherFields.getWindTf())
            .asRequired("Wind is required")
            .withConverter(new StringToDoubleConverter("Should be a number"))
            .withValidator(windSpeed -> windSpeed >= 0, "Wind speed should a positive number")
            .bind(
                    weather -> weather.getWeatherProperty().getWindSpeed(),
                    (weather, windSpeed) -> weather.getWeatherProperty().setWindSpeed(windSpeed));

    binder.forField(weatherFields.getHumidityTf())
            .asRequired("Humidity is required")
            .withConverter(new StringToIntegerConverter("Should be an integer"))
            .withValidator(humidity -> (humidity >= 0 && humidity <= 100),
                    "Humidity should be between 0 and 100")
            .bind(
                    weather -> weather.getWeatherProperty().getHumidityPercentage(),
                    (weather, humidity) ->
                            weather.getWeatherProperty().setHumidityPercentage(humidity));

    binder.forField(weatherFields.getPressureTf())
            .asRequired("The pressure is required")
            .withConverter(new StringToDoubleConverter("Should be a number"))
            .withValidator(pressure -> pressure >= 0, "Pressure should be a positive number")
            .bind(
                    weather -> weather.getWeatherProperty().getPressure(),
                    (weather, pressure) -> weather.getWeatherProperty().setPressure(pressure));

    binder.forField(weatherFields.getVisibilityTf())
            .asRequired("The visibility is required")
            .withConverter(new StringToIntegerConverter("Should be a number"))
            .withValidator(visibility -> visibility >= 0, "Visibility should be a positive number")
            .bind(
                    weather -> weather.getWeatherProperty().getVisibility(),
                    (weather, visibility) ->
                            weather.getWeatherProperty().setVisibility(visibility));

    binder.forField(weatherFields.getTemperatureTf())
            .asRequired("Temperature is required")
            .withConverter(new StringToIntegerConverter("Should be a number"))
            .bind(
                    weather -> weather.getWeatherProperty().getTemperature(),
                    (weather, temperature) ->
                            weather.getWeatherProperty().setTemperature(temperature));
  }

  public Binder<Weather> getBinder() {
    return binder;
  }
}
