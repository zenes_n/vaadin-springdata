package experiments.bootvaadin.ui.components;

import com.vaadin.ui.Grid;
import experiments.bootvaadin.model.Weather;

public class WeatherGrid extends Grid<Weather> {
  private WeatherFields fields = new WeatherFields();
  private boolean modifiable = false;

  public WeatherGrid() {
    super();
    this.setSelectionMode(Grid.SelectionMode.MULTI);
    this.setWidth("100%");
    this.setHeight("650px");
    addColumns();
  }

  private void addColumns() {
    this.addColumn(Weather::getDate)
            .setCaption("Date")
            .setId("date")
            .isSortable();
    this.addColumn(weather -> weather.getWeatherProperty().getWindSpeed())
            .setCaption("Wind speed")
            .setId("windSpeed")
            .isSortable();
    this.addColumn(weather -> weather.getWeatherProperty().getHumidityPercentage())
            .setCaption("Humidity percentage")
            .setId("humidity")
            .isSortable();
    this.addColumn(weather -> weather.getWeatherProperty().getPressure())
            .setCaption("Pressure")
            .setId("pressure")
            .isSortable();
    this.addColumn(weather -> weather.getWeatherProperty().getTemperature())
            .setCaption("Temperature")
            .setId("temperature")
            .isSortable();
    this.addColumn(weather -> weather.getWeatherProperty().getVisibility())
            .setCaption("Visibility")
            .setId("visibility")
            .isSortable();
  }

  public boolean isModifiable() {
    return modifiable;
  }

  public void setModifiable(boolean modifiable) {
    this.modifiable = modifiable;
  }
}
