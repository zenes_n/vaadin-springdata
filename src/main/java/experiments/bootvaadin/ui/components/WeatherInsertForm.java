package experiments.bootvaadin.ui.components;

import com.vaadin.data.Binder;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import experiments.bootvaadin.model.Weather;

public class WeatherInsertForm extends FormLayout {

  private Button add = new Button("Add");
  private Button cancel = new Button("Cancel");
  private WeatherFields weatherFields = new WeatherFields();
  private Binder<Weather> binder;

  private Listener saveHandler;

  public WeatherInsertForm() {
    VerticalLayout layout = new VerticalLayout();
    layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

    layout.addComponent(weatherFields);
    WeatherFieldsBinder weatherBinder = new WeatherFieldsBinder(weatherFields);
    this.binder = weatherBinder.getBinder();
    this.binder.addValueChangeListener(event -> {
      this.add.setEnabled(this.binder.isValid());
    });
    addButtonsLayout(layout);
    this.addComponent(layout);
  }

  private void addButtonsLayout(VerticalLayout layout) {
    HorizontalLayout buttonsLayout = new HorizontalLayout();

    add.setEnabled(false);
    add.setStyleName(ValoTheme.BUTTON_PRIMARY);

    buttonsLayout.addComponents(add, cancel);
    layout.addComponent(buttonsLayout);
    layout.setComponentAlignment(buttonsLayout, Alignment.TOP_LEFT);
  }


  public Button getAdd() {
    return add;
  }

  public Button getCancel() {
    return cancel;
  }

  public WeatherFields getWeatherFields() {
    return weatherFields;
  }

  public Binder<Weather> getBinder() {
    return binder;
  }
}
