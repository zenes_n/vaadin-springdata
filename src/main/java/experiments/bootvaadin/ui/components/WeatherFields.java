package experiments.bootvaadin.ui.components;

import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class WeatherFields extends VerticalLayout {
  private DateField dateField;
  private TextField windTf;
  private TextField humidityTf;
  private TextField pressureTf;
  private TextField temperatureTf;
  private TextField visibilityTf;

  WeatherFields() {
    dateField = new DateField("Date");
    dateField.setRequiredIndicatorVisible(true);
    dateField.setWidth("100%");

    windTf = new TextField("Wind speed");
    windTf.setRequiredIndicatorVisible(true);
    windTf.setWidth("100%");

    humidityTf = new TextField("Humidity (%)");
    humidityTf.setRequiredIndicatorVisible(true);
    humidityTf.setWidth("100%");

    pressureTf = new TextField("Pressure");
    pressureTf.setRequiredIndicatorVisible(true);
    pressureTf.setWidth("100%");

    temperatureTf = new TextField("Temperature");
    temperatureTf.setRequiredIndicatorVisible(true);
    temperatureTf.setWidth("100%");

    visibilityTf = new TextField("Visibility");
    visibilityTf.setRequiredIndicatorVisible(true);
    visibilityTf.setWidth("100%");

    this.addComponents(
            dateField,
            windTf,
            humidityTf,
            pressureTf,
            temperatureTf,
            visibilityTf
    );

    this.setMargin(false);
  }

  public void clearFields() {
    windTf.setValue("");
    humidityTf.setValue("");
    pressureTf.setValue("");
    temperatureTf.setValue("");
    visibilityTf.setValue("");
  }

  /*
   * Getters and setters
   * */

  public DateField getDateField() {
    return dateField;
  }

  public TextField getWindTf() {
    return windTf;
  }

  public TextField getHumidityTf() {
    return humidityTf;
  }

  public TextField getPressureTf() {
    return pressureTf;
  }

  public TextField getTemperatureTf() {
    return temperatureTf;
  }

  public TextField getVisibilityTf() {
    return visibilityTf;
  }
}
