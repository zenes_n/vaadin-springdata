package experiments.bootvaadin.ui.components;

import com.vaadin.ui.Window;

public class WeatherInsertWindow extends Window {
  private WeatherInsertForm weatherInsertForm;

  public WeatherInsertWindow(WeatherInsertForm weatherInsertForm) {
    setContent(weatherInsertForm);
    setResizable(false);
    setDraggable(false);
    setClosable(true);
    setModal(true);
    setWidth("400px");
    setPosition(50, 50);
    weatherInsertForm.getCancel().addClickListener(clickEvent -> close());
    center();

    this.weatherInsertForm = weatherInsertForm;
  }

  public WeatherInsertForm getWeatherInsertForm() {
    return weatherInsertForm;
  }

  public void setWeatherInsertForm(WeatherInsertForm weatherInsertForm) {
    this.weatherInsertForm = weatherInsertForm;
  }
}
