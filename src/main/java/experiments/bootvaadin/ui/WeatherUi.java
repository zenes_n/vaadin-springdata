package experiments.bootvaadin.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import experiments.bootvaadin.model.Weather;
import experiments.bootvaadin.service.WeatherService;
import experiments.bootvaadin.ui.components.WeatherGrid;
import experiments.bootvaadin.ui.components.WeatherInsertForm;
import experiments.bootvaadin.ui.components.WeatherInsertWindow;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

@SpringUI
@Theme("valo")
public class WeatherUi extends UI {
  @Autowired
  WeatherService weatherService;
  private VerticalLayout layout;
  private WeatherInsertForm weatherInsertForm = new WeatherInsertForm();
  private WeatherInsertWindow weatherInsertWindow;
  private WeatherGrid weatherGrid;
  private Button delete;
  private Button edit;

  @Override
  protected void init(VaadinRequest vaadinRequest) {
    setupLayout();
    addHeader(layout);
    addBody(layout);
  }

  private void setupLayout() {
    layout = new VerticalLayout();
    layout.setWidth("100%");
    setContent(layout);
  }

  private void addHeader(VerticalLayout layout) {
    HorizontalLayout horizontalLayout = new HorizontalLayout();
    horizontalLayout.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);

    Label header = new Label("Weather");
    Label headerDescription = new Label("a Java Class");

    header.addStyleName(ValoTheme.LABEL_H1);
    headerDescription.addStyleName(ValoTheme.LABEL_SMALL);

    horizontalLayout.addComponent(header);
    horizontalLayout.addComponent(headerDescription);
    layout.addComponent(horizontalLayout);
  }

  private void addBody(VerticalLayout layout) {
    HorizontalLayout controlButtonsLayout = new HorizontalLayout();
    HorizontalLayout gridLayout = new HorizontalLayout();

    gridLayout.setWidth("100%");
    addControlButtons(controlButtonsLayout);
    addGrid(gridLayout);

    layout.addComponent(controlButtonsLayout);
    layout.addComponent(gridLayout);
    layout.setComponentAlignment(gridLayout, Alignment.MIDDLE_CENTER);
  }

  private void addControlButtons(HorizontalLayout gridLayout) {
    Button insert = new Button("Insert");
    edit = new Button("Edit");
    delete = new Button("Delete");

    insert.addClickListener(clickEvent -> insertButtonAction(clickEvent));
    insert.addStyleName(ValoTheme.BUTTON_PRIMARY);

    edit.addClickListener(clickEvent -> editButtonAction());
    delete.addClickListener(clickEvent -> deleteButtonAction());

    gridLayout.addComponent(insert);
    gridLayout.addComponent(edit);
    gridLayout.addComponent(delete);

    gridLayout.setComponentAlignment(edit, Alignment.MIDDLE_CENTER);
  }

  private void insertButtonAction(Button.ClickEvent event) {
    weatherInsertWindow = new WeatherInsertWindow(weatherInsertForm);

    UI.getCurrent().addWindow(weatherInsertWindow);

    Weather weather = new Weather();
    weatherInsertForm.getBinder().setBean(weather);
    weatherInsertForm.getWeatherFields().clearFields();
    weatherInsertForm.getAdd().addClickListener(
            clickEvent -> addButtonAction(weatherInsertWindow, weather)
    );

  }

  private void addButtonAction(WeatherInsertWindow weatherInsertWindow, Weather weather) {
    weather = weatherInsertWindow.getWeatherInsertForm().getBinder().getBean();
    weatherService.saveWeather(weather);
    weatherInsertWindow.close();
    Notification.show("Succes", "Record saved",
            Notification.Type.TRAY_NOTIFICATION).setStyleName(ValoTheme.NOTIFICATION_SUCCESS);
    updateGrid();
  }

  private void deleteButtonAction() {
    Set<Weather> weatherForDeletion = getGrid().getSelectedItems();

    int deletedCount = weatherForDeletion.size();

    for (Weather weather : weatherForDeletion) {
      weatherService.deleteWeather(weather);
    }

    updateGrid();

    if (deletedCount > 0) {
      Notification.show(deletedCount + " record(s) deleted",
              Notification.Type.TRAY_NOTIFICATION).setStyleName(ValoTheme.NOTIFICATION_SUCCESS);
    } else {
      Notification.show("Nothing to delete",
              Notification.Type.TRAY_NOTIFICATION).setStyleName(ValoTheme.NOTIFICATION_WARNING);
    }
  }

  private void editButtonAction() {

    Set<Weather> weatherSet = this.getGrid().getSelectedItems();

    if (weatherSet.size() > 1) {

      Notification.show("You can edit only one record a time",
              Notification.Type.TRAY_NOTIFICATION).setStyleName(ValoTheme.NOTIFICATION_WARNING);
      return;

    } else if (weatherSet.size() == 0) {

      Notification.show("Nothing to edit",
              Notification.Type.TRAY_NOTIFICATION).setStyleName(ValoTheme.NOTIFICATION_WARNING);
      return;

    }

    for (Weather weather : weatherSet) {

      WeatherInsertForm weatherEditForm = new WeatherInsertForm();
      weatherEditForm.getAdd().setCaption("Edit");
      weatherInsertWindow = new WeatherInsertWindow(weatherEditForm);

      UI.getCurrent().addWindow(weatherInsertWindow);

      weatherEditForm.getBinder().setBean(weather);
      weatherEditForm.getAdd().addClickListener(
              clickEvent -> addButtonAction(weatherInsertWindow, weather)
      );
    }
  }

  private void updateGrid() {
    List<Weather> weatherDbElements = weatherService.getAllWeathers();
    getGrid().setItems(weatherDbElements);
    ListDataProvider<Weather> dataProvider = (ListDataProvider<Weather>) getGrid().getDataProvider();
    dataProvider.addFilter(row -> row.getWeatherProperty().getPressure() == 3);
  }

  private void addGrid(HorizontalLayout gridLayout) {
    weatherGrid = new WeatherGrid();
    updateGrid();
    enableWeatherSelectionListener();
    gridLayout.addComponents(weatherGrid);
  }

  private void enableWeatherSelectionListener() {
    weatherGrid.addSelectionListener(selectionEvent -> {
      this.delete.setStyleName(ValoTheme.BUTTON_DANGER);
      this.delete.setCaption("Delete " + weatherGrid.getSelectedItems().size() + " item(s)");

      if (weatherGrid.getSelectedItems().size() == 0) {
        this.delete.setCaption("Delete");
        this.delete.setStyleName(null);
      }
    });
  }

  private Grid<Weather> getGrid() {
    return this.weatherGrid;
  }

}
