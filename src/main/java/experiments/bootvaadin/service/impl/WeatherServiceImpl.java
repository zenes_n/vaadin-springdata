package experiments.bootvaadin.service.impl;

import experiments.bootvaadin.model.Weather;
import experiments.bootvaadin.repository.WeatherRepository;
import experiments.bootvaadin.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class WeatherServiceImpl implements WeatherService {
  @Autowired
  private WeatherRepository repository;

  @Override
  public Weather saveWeather(Weather weather) {
    return repository.save(weather);
  }

  @Override
  public List<Weather> getAllWeathers() {
    return repository.findAll();
  }

  @Override
  @Transactional(rollbackFor = {Exception.class})
  public void deleteWeather(Weather weather) {
    repository.delete(weather);
  }
}
