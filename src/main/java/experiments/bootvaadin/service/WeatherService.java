package experiments.bootvaadin.service;

import experiments.bootvaadin.model.Weather;

import java.util.List;

public interface WeatherService {
  Weather saveWeather(Weather weather);

  List<Weather> getAllWeathers();

  void deleteWeather(Weather weather);
}
