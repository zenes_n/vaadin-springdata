package experiments.bootvaadin.repository;

import experiments.bootvaadin.model.Weather;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeatherRepository extends JpaRepository<Weather, Long> {
}
