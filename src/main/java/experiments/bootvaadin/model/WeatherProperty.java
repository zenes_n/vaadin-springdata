package experiments.bootvaadin.model;

import javax.persistence.*;

@Entity
@Table(name = "weather_property")
public class WeatherProperty {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "weather_property_id")
  private long id;

  private double windSpeed;
  private int temperature;
  private int humidityPercentage;
  private double pressure;
  private int visibility;

  public WeatherProperty() {
  }

  public WeatherProperty(WeatherProperty weatherProperty) {
    this.setWindSpeed(weatherProperty.getWindSpeed());
    this.setTemperature(weatherProperty.getTemperature());
    this.setHumidityPercentage(weatherProperty.getHumidityPercentage());
    this.setPressure(weatherProperty.getPressure());
    this.setVisibility(weatherProperty.getVisibility());
  }

  public WeatherProperty(double windSpeed,
                         int temperature,
                         int humidityPercentage,
                         double pressure,
                         int visibility) {

    this.windSpeed = windSpeed;
    this.temperature = temperature;
    this.humidityPercentage = humidityPercentage;
    this.pressure = pressure;
    this.visibility = visibility;
  }

  public boolean equals(WeatherProperty weatherProperty) {
    if (Double.compare(this.getWindSpeed(), weatherProperty.getWindSpeed()) != 0) {
      return false;
    }

    if (this.getTemperature() != weatherProperty.getTemperature()) {
      return false;
    }

    if (this.getHumidityPercentage() != weatherProperty.getHumidityPercentage()) {
      return false;
    }

    if (this.getPressure() != weatherProperty.getPressure()) {
      return false;
    }

    return (this.getVisibility() == weatherProperty.getVisibility());

  }

  public boolean isWarm() {
    return temperature > 18;
  }

  public boolean isCold() {
    return temperature < 7;
  }

  public boolean isWindy() {
    return windSpeed > 20;
  }

  /*
   * Getters and setters
   * */

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Double getWindSpeed() {
    return windSpeed;
  }

  public void setWindSpeed(Double windSpeed) {
    this.windSpeed = windSpeed;
  }

  public int getTemperature() {
    return temperature;
  }

  public void setTemperature(int temperature) {
    this.temperature = temperature;
  }

  public int getHumidityPercentage() {
    return humidityPercentage;
  }

  public void setHumidityPercentage(int humidityPercentage) {
    this.humidityPercentage = humidityPercentage;
  }

  public double getPressure() {
    return pressure;
  }

  public void setPressure(double pressure) {
    this.pressure = pressure;
  }

  public int getVisibility() {
    return visibility;
  }

  public void setVisibility(int visibility) {
    this.visibility = visibility;
  }
}
