package experiments.bootvaadin.model.validation;


import experiments.bootvaadin.model.Weather;

public class EarthWeatherValidator implements WeatherValidator {

  @Override
  public boolean isValid(Weather weather) {
    if (weather.getWeatherProperty().getHumidityPercentage() < 0
            || weather.getWeatherProperty().getHumidityPercentage() > 100) {
      return false;
    }

    if (weather.getWeatherProperty().getWindSpeed() < 0) {
      return false;
    }

    if (weather.getWeatherProperty().getPressure() < 0) {
      return false;
    }

    return weather.getWeatherProperty().getVisibility() > 0;
  }

}
