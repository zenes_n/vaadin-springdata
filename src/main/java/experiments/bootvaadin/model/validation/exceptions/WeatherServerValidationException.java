package experiments.bootvaadin.model.validation.exceptions;

public class WeatherServerValidationException extends Exception {
  @Override
  public String getMessage() {
    return "Server validation failed";
  }
}
