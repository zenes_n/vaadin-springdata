package experiments.bootvaadin.model.validation;


import experiments.bootvaadin.model.Weather;

interface WeatherValidator {
  boolean isValid(Weather weather);
}
