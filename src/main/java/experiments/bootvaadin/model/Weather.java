package experiments.bootvaadin.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "weather")
public class Weather {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  private LocalDate date;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinColumn(name = "weather_property_id")
  private WeatherProperty weatherProperty;

  /*
   * Constructors
   * */

  public Weather() {
    weatherProperty = new WeatherProperty();
  }

  public Weather(LocalDate date, WeatherProperty weatherProperty) {
    this.setDate(date);
    this.setWeatherProperty(weatherProperty);
  }

  public Weather(Weather weather) {
    this.setDate(weather.getDate());
    this.setWeatherProperty(weather.getWeatherProperty());
  }

  /*
   * Class methods
   * */

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public WeatherProperty getWeatherProperty() {
    return this.weatherProperty;
  }

  public void setWeatherProperty(WeatherProperty weatherProperty) {
    this.weatherProperty = weatherProperty;
  }
}
