package experiments.bootvaadin;

import experiments.bootvaadin.service.WeatherService;
import experiments.bootvaadin.service.impl.WeatherServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BootVaadinApplication {
  public static void main(String[] args) {
    SpringApplication.run(BootVaadinApplication.class, args);
  }

  @Bean
  WeatherService getWeatherService() {
    return new WeatherServiceImpl();
  }
}
